from os import listdir, getcwd
from os.path import join
import nltk
from tensorflow.keras.utils import Progbar
from ellogon import tokeniser


class Preprocessor:

    def __init__(self, folders):
        self.folders = folders

    def preprocess(self, lines):
        raise NotImplementedError

    def apply_preprocessing(self):
        for folder in self.folders:
            print(f"Processing folder {folder}")
            files = listdir(folder)
            bar = Progbar(len(files))
            for file in files:
                filepath = join(folder, file)
                with open(filepath, "r") as f:
                    lines = f.readlines()
                preprocessed_lines = self.preprocess(lines=lines)
                print(f"Writing to file {filepath}")
                with open(filepath, "w") as f:
                    content = "\n".join(preprocessed_lines)
                    f.write(content)
                bar.add(1)


class CustomPreprocessor(Preprocessor):
    def __init__(self, folders):
        super(CustomPreprocessor, self).__init__(folders=folders)
        # regex_tokenizer = nltk.RegexpTokenizer("\w+")
        self.regex_tokenizer = nltk.RegexpTokenizer("[\w']+|[.,!?;]")

    def normalize_text(self, text):
        # lowercase text
        text = str(text).lower()
        # remove non-UTF
        text = text.encode("utf-8", "ignore").decode()
        # remove punktuation symbols
        text = " ".join(self.regex_tokenizer.tokenize(text))
        return text

    def preprocess(self, lines):
        preprocessed_lines = []
        for line in lines:
            line = self.normalize_text(text=line)
            preprocessed_lines.append(line)
        return preprocessed_lines


class EllogonPreprocessor(Preprocessor):

    def __init__(self, folders):
        super(EllogonPreprocessor, self).__init__(folders=folders)
        print("Using EllogonPreprocessor")

    def preprocess(self, lines):
        preprocessed_lines = []
        for line in lines:
            sentence = tokeniser.tokenise(line)
            sentence = [token for sen in sentence for token in sen]
            sentence = " ".join(sentence)
            print(f"Preprocessed sentence: {sentence}")
            preprocessed_lines.append(sentence)
        return preprocessed_lines


if __name__ == '__main__':
    prep = EllogonPreprocessor(folders=["/var/mysecondstore/debatelab/corpora/datasets"])
    prep.apply_preprocessing()