import logging
from collections.abc import Iterator
from typing import List, AnyStr

import torch
from torch.utils.data import IterableDataset
from torch.utils.data.dataset import T_co

from src.greek_models.greek_tokenizers import GreekTokenizer
from src.utils.config import AppConfig


class DatasetIterator(Iterator):
    def __init__(self, dataset, next_dataset_func):
        self.dataset = dataset
        self.next_dataset_func = next_dataset_func
        self.do_reset = None
        self._populate_iterator()

    def _populate_iterator(self):
        self.data_iter = iter(self.dataset)

    def __next__(self):
        try:
            return next(self.data_iter)
        except StopIteration:
            self.dataset = self.next_dataset_func()
            if self.dataset is not None:
                self._populate_iterator()
                return self.__next__()
            else:
                self.do_reset = True
                raise StopIteration


class GreekDataset(IterableDataset):

    def __init__(self, greek_tokenizer: GreekTokenizer, val: bool = False):
        super(GreekDataset, self).__init__()
        self.greek_tokenizer: GreekTokenizer = greek_tokenizer
        self.app_config: AppConfig = self.greek_tokenizer.app_config
        self.app_logger: logging.Logger = self.app_config.app_logger
        self.max_length = self.greek_tokenizer.max_length
        self.epochs = self.app_config.properties["model"]["epochs"]
        self.tokenizer = self.greek_tokenizer.load()
        self.paths: List[AnyStr] = self.greek_tokenizer.train_file_paths if not val else \
            self.greek_tokenizer.val_file_paths
        self.current_file_idx = 0
        self.lines = []
        self.dataset_iterator = DatasetIterator([], self.get_next_dataset)
        self.lines_dict = self._lines_per_file()

    def preprocess(self, text):
        self.app_logger.debug(f"Tokenizing text {text}")
        encoding = self.tokenizer(text.strip("\n"), add_special_tokens=True, truncation=True,
                                  max_length=self.max_length)
        return torch.tensor(encoding["input_ids"])

    def reset_dataset(self):
        self.app_logger.info("Resetting datasets for next epoch")
        self.current_file_idx = 0
        self.dataset_iterator = DatasetIterator([], self.get_next_dataset)

    def get_next_dataset(self):
        self.app_logger.info(f"Loading new dataset")
        self.current_file_idx += 1
        if self.current_file_idx >= len(self.paths):
            return None
        # self.current_file_idx = (self.current_file_idx + 1) % len(self.paths)
        return self.read_file()

    def read_file(self):
        filepath = self.paths[self.current_file_idx]
        self.app_logger.info(f"Loading file {filepath}")
        # read and return contents
        with open(filepath) as fptr:
            self.app_logger.debug("Reading file lines and tokenizing text")
            self.lines = [self.preprocess(text=line.strip()) for line in fptr.readlines() if line.strip()]
        return self.lines

    def _lines_per_file(self):
        lines_dict = {}
        for idx, filepath in enumerate(self.paths):
            with open(filepath, "r") as f:
                lines = f.readlines()
                num_lines = len(lines)
                lines_dict[idx] = num_lines
        return lines_dict

    def __iter__(self):
        if self.dataset_iterator.do_reset:
            self.reset_dataset()
        return self.dataset_iterator

    def __len__(self):
        return self.lines_dict[self.current_file_idx] if self.current_file_idx < len(self.paths) else 0

    def __getitem__(self, index) -> T_co:
        self.dataset_iterator.__next__()


def test_dataset():
    app_config = AppConfig()
    greek_tokenizer = GreekTokenizer(app_config=app_config)
    greek_tokenizer.load()
    greek_dataset = GreekDataset(greek_tokenizer=greek_tokenizer)
    # two iterations on the dataset
    for instance in greek_dataset:
        print(instance)
    print("=====================================================================")
    for instance in greek_dataset:
        print(instance)


if __name__ == '__main__':
    test_dataset()
