import traceback
from datetime import datetime
from os import listdir
from os.path import join, isfile
from typing import Union, Dict, AnyStr
from xml.etree import ElementTree

# import pandas as pd
from src.utils import utils
from src.utils.config import AppConfig, ElasticSearchConfig


class DatasetCreator:
    """
        Class which reads dataset files (currently in txt, xml and tsv format) and creates txt files with the
        document's sentences.
    """

    def __init__(self, resources_folder):
        """
        DatasetCreator constructor

        Args
            | resources_folder (str): the path to the resources folder where all datasets are stored
        """
        self.resources_folder = resources_folder
        # new path where the processed files will be stored
        self.dataset_path = join(self.resources_folder, "datasets")

    def preprocess_data(self, data: Union[AnyStr, Dict], sentences=None, kind=None):
        """
        Public function which preprocess the given dataset, based on the data and kind parameters

        Args
            | data(str or dict): path to the data files or dict containing the documents. The dictionary of data is
            used by the ElasticSearch
            | sentences (list): a list of the sentences of the document. When the method is called sentences should be
            None and they are populated recursively when checking all the dataset's folders.
            | kind (str): the file kind, i.e. text, tsv or xml
        """
        if type(data) == str:
            self._preprocess_from_files(parent_path=data, sentences=sentences, kind=kind)
        else:
            self._preprocess_from_elasticsearch(data=data)

    def _preprocess_from_elasticsearch(self, data: dict):
        """
        Preprocessing of retrieved data from Elasticsearch. All documents' contents are preprocessed and written
        into a file, where each line is one sentence

        Args
            | data (dict): dictionary containing the retrieved data
        """
        data = data["docs"]
        for document in data:
            content = document["_source"]["content"]
            sentences = self._get_sentences(content=content)
            timestamp = datetime.now()
            utils.append_file(sentences=sentences, filepath=join(self.dataset_path, f"elasticsearch_{timestamp}"))

    def _preprocess_from_files(self, parent_path: AnyStr, sentences=None, kind="text"):
        """
        Preprocessing from files in the file system. File types can be txt, tsv or xml. The documents are split into
        sentences and written into a new file

        Args
            | parent_path (str): the path to the dataset's root folder
            | sentences (list): a list of the sentences of the document. When the method is called sentences should be
            None and they are populated recursively when checking all the dataset's folders.
            | kind (str): the file kind, i.e. text, tsv or xml
        """
        if not sentences:
            sentences = []
        folder_contents = listdir(parent_path)
        for filename in folder_contents:
            filepath = join(parent_path, filename)
            if isfile(filepath):
                print(f"Processing file {filename}")
                if kind == "text":
                    self._preprocess_txt_file(filepath=filepath, filename=filename)
                elif kind == "tsv":
                    self._preprocess_tsv_file(filepath=filepath, filename=filename)
                else:
                    self._preprocess_xml_file(filepath=filepath, filename=filename)
            else:
                print(f"Processing folder {filename}")
                self._preprocess_from_files(parent_path=filepath, sentences=sentences, kind=kind)

    def _preprocess_txt_file(self, filepath, filename):
        """
        Preprocessing of txt files. For now the respective datasets are: EU parliament data and OSCAR dataset

        Args
            | filepath (str): full path of the respective file
            | filename (str): the name of the file
        """
        with open(filepath, "r") as f:
            for line in f:
                sentences = self._get_sentences(content=line)
                utils.append_file(sentences=sentences, filepath=join(self.dataset_path, filename))

    def _preprocess_tsv_file(self, filepath, filename):
        """
        Preprocessing of tsv files (e.g. TED talks)

        Args
            | filepath (str): full path of the respective file
            | filename (str): the name of the file
        """
        # df = pd.read_csv(filepath, sep="\t", index_col=None, header=None, error_bad_lines=False)
        # greek_sentences = list(df[1])
        with open(filepath, "r") as f:
            for line in f:
                content = line.split("\t")[1]
                sentences = self._get_sentences(content=content)
                utils.append_file(sentences=sentences, filepath=join(self.dataset_path, filename))

    def _preprocess_xml_file(self, filepath, filename):
        """
        Preprocessing of xml files (e.g. Wikipedia)

        Args
            | filepath (str): full path of the respective file
            | filename (str): the name of the file
        """
        self._add_parent_tag(filepath=filepath)
        root = ElementTree.parse(filepath).getroot()
        for type_tag in root.findall("doc"):
            content = type_tag.text
            sentences = self._get_sentences(content=content)
            utils.append_file(sentences=sentences, filepath=join(self.dataset_path, filename))

    @staticmethod
    def _get_sentences(content: str):
        """
        Function which preprocess the document's sentences. The document's content is split using the ellogon library
        and then the tuples are joint again into a string.

        Args
            | content (str): the document's content

        Returns
            | list: a list of the processed sentences
        """
        tokenized_sentences = utils.tokenize(content=content)
        return utils.join_sentences(tokenized_sentences=tokenized_sentences)

    @staticmethod
    def _add_parent_tag(filepath: AnyStr):
        """
        Function to preprocess wikipedia files. Adds a root tag in order to retrieve all doc tags. Checks if the
        parent tag already exists, otherwise the file is updated with the parent tag <wikipedia>

        Args
            | filepath (str): full path of the xml file
        """
        with open(filepath, "r") as f:
            content = f.read()
        if not content.startswith("<wikipedia>"):
            content = "<wikipedia>\n" + content + "\n</wikipedia>"
            with open(filepath, "w") as f:
                f.write(content)


def base_dataset(resources_folder: AnyStr, dataset_creator: DatasetCreator):
    eu_parliament_folder = join(resources_folder, "eu_parliament")
    oscar_folder = join(resources_folder, "oscar")
    wikipedia_folder = join(resources_folder, "wikipedia")
    print("Processing EU Parliament data")
    dataset_creator.preprocess_data(data=eu_parliament_folder, kind="text")
    print("Processing OSCAR data")
    dataset_creator.preprocess_data(data=oscar_folder, kind="text")
    print("Processing Wikipedia data")
    dataset_creator.preprocess_data(data=wikipedia_folder, kind="xml")


def news_dataset(elastic_config: ElasticSearchConfig, dataset_creator: DatasetCreator):
    date = datetime.now()
    data = elastic_config.retrieve_documents(previous_date=date)
    dataset_creator.preprocess_data(data=data)


def ted_dataset(resources_folder: AnyStr, dataset_creator: DatasetCreator):
    parent_path = join(resources_folder, "ted")
    dataset_creator.preprocess_data(data=parent_path, kind="tsv")


def collect_datasets():
    app_config: AppConfig = AppConfig()
    resources_folder: AnyStr = app_config.resources_path
    logger = app_config.app_logger
    properties: Dict = app_config.properties
    elastic_config: ElasticSearchConfig = ElasticSearchConfig(properties=properties)
    try:
        dataset_creator: DatasetCreator = DatasetCreator(resources_folder=resources_folder)
        base_dataset(resources_folder, dataset_creator)
        news_dataset(elastic_config=elastic_config, dataset_creator=dataset_creator)
        ted_dataset(resources_folder=resources_folder, dataset_creator=dataset_creator)
        utils.combine_files(parent_folder=join(resources_folder, "datasets"), from_file="ted2.tsv",
                            to_file="ted1.tsv")
        elastic_config.stop()
    except(BaseException, Exception):
        logger.error(traceback)
        elastic_config.stop()
    finally:
        elastic_config.stop()
