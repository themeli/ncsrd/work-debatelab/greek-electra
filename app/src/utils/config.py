import logging
import threading
from datetime import datetime
from os import getcwd, mkdir, environ
from os.path import join, exists
from pathlib import Path
from typing import Dict, AnyStr, List

import torch
import yaml
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from sshtunnel import SSHTunnelForwarder


class AppConfig:

    def __init__(self):
        self._create_paths()
        self._configure_device()
        self.properties: Dict = self._load_properties()
        self._create_paths_from_properties()
        self.app_logger: logging.Logger = self._config_logger()
        self._config_datasets()

    def _configure_device(self):
        """
        Reads the environmental variable CUDA_VISIBLE_DEVICES in order to initialize the device to be used
        in the training
        """
        if torch.cuda.is_available():
            devices = environ.get("CUDA_VISIBLE_DEVICES", 0)
            if type(devices) == str:
                devices = devices.split(",")
                self.device_name = f"cuda:{devices[0].strip()}"
            else:
                self.device_name = f"cuda:{devices}"
        else:
            self.device_name = "cpu"

    def _create_paths(self):
        self.app_path = Path(__file__).parent.parent
        self.resources_path = join(self.app_path, "resources")
        self.output_path = join(self.app_path, "output")
        self.models_path = join(self.output_path, "models")
        self.logs_path = join(self.output_path, "logs")
        self.results_path = join(self.output_path, "results")
        self.tokenizer_path = join(self.models_path, "tokenizer")
        self.transformer_path = join(self.models_path, "transformer")

        if not exists(self.output_path):
            mkdir(self.output_path)
        if not exists(self.logs_path):
            mkdir(self.logs_path)
        if not exists(self.models_path):
            mkdir(self.models_path)
        if not exists(self.results_path):
            mkdir(self.results_path)
        if not exists(self.tokenizer_path):
            mkdir(self.tokenizer_path)
        if not exists(self.transformer_path):
            mkdir(self.transformer_path)

    def _load_properties(self) -> Dict:
        """
        Loads the configuration file from the resources folder

        Returns
            dict: the application properties
        """
        self.properties_file = "properties.yaml"
        self.example_properties = "example_properties.yaml"
        path_to_properties = join(self.resources_path, self.properties_file)
        path_to_example_properties = join(self.resources_path, self.example_properties)
        final_path = path_to_properties if exists(path_to_properties) else path_to_example_properties
        with open(final_path, "r") as f:
            properties = yaml.safe_load(f.read())
        return properties

    def _create_paths_from_properties(self):
        self.cache_dir = self.properties["datasets"]["cache_dir"]
        self.tokenized_data_path = self.properties["datasets"]["tokenized_data"]
        self.train_tokenized_datasets_path = join(self.tokenized_data_path, "train")
        self.test_tokenized_datasets_path = join(self.tokenized_data_path, "test")
        model_name = self.properties["model"]["name"]
        model_path = join(self.transformer_path, model_name)

        if not exists(self.cache_dir):
            mkdir(self.cache_dir)
        if not exists(self.tokenized_data_path):
            mkdir(self.tokenized_data_path)
        if not exists(self.train_tokenized_datasets_path):
            mkdir(self.train_tokenized_datasets_path)
        if not exists(self.test_tokenized_datasets_path):
            mkdir(self.test_tokenized_datasets_path)
        if not exists(model_path):
            mkdir(model_path)

    def _config_logger(self) -> logging.Logger:
        """
        Configures the application logger

        Returns
            logger: the initialized logger
        """
        logging.basicConfig(level=logging.INFO)
        timestamp = datetime.now().strftime('%Y%m%d-%H%M%S')
        self.log_filename = f"logs_{timestamp}.log"
        log_formatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
        program_logger: logging.Logger = logging.getLogger("transformers")
        program_logger.setLevel(logging.DEBUG)
        file_handler = logging.FileHandler(f"{self.logs_path}/{self.log_filename}")
        file_handler.setFormatter(log_formatter)
        program_logger.addHandler(file_handler)

        console_handler = logging.StreamHandler()
        console_handler.setFormatter(log_formatter)
        program_logger.addHandler(console_handler)
        return program_logger

    def _config_datasets(self):
        dataset_properties = self.properties["datasets"]
        self.train_initial_datasets: List[Dict] = dataset_properties.get("train")
        self.test_initial_datasets: List[Dict] = dataset_properties.get("eval")
        self.train_initial_dataset_paths, self.test_initial_dataset_paths = [], []
        self.train_dataset_names, self.test_dataset_names = [], []
        if self.train_initial_datasets:
            for dataset in self.train_initial_datasets:
                self.train_dataset_names.append(dataset["name"])
                self.train_initial_dataset_paths.append(dataset["path"])
        if self.test_initial_datasets:
            for dataset in self.test_initial_datasets:
                self.test_dataset_names.append(dataset["name"])
                self.test_initial_dataset_paths.append(dataset["path"])


class ElasticSearchConfig:
    """
    Class to configure an Elasticsearch Client
    """

    def __init__(self, properties: Dict):
        """
        Constructor of the ElasticSearchConfig class

        Args
            | properties (dict): a dictionary with the configuration parameters
        """
        properties: Dict = properties.get("elasticsearch", None)
        if properties:
            self.username: AnyStr = properties["username"]
            self.password: AnyStr = properties["password"]
            self.host: AnyStr = properties["host"]
            self.port: int = properties["port"]
            self.ssh_port: int = properties["ssh"]["port"]
            self.ssh_username: AnyStr = properties["ssh"]["username"]
            self.ssh_password: AnyStr = properties["ssh"]["password"]
            self.ssh_key: AnyStr = properties["ssh"]["key_path"]
            self.connect: bool = properties["connect"]
            try:
                self._init_ssh_tunnel()
                self._init_elasticsearch_client()
                self.connected = True
            except (BaseException, Exception):
                self.connected = False
        else:
            raise Exception("Provide configuration for elasticsearch. Check in resourses/example_properties.yaml"
                            " for the necessary parameters")

    def _init_elasticsearch_client(self, timeout=60):
        """
        Initialization of the Elasticsearch client

        Args
            timeout (int): optional parameter to define the timeout for the connection
        """
        self.elasticsearch_client = Elasticsearch([{
            'host': "localhost",
            'port': self.tunnel.local_bind_port,
            'http_auth': (self.username, self.password)
        }], timeout=timeout)

    def _init_ssh_tunnel(self):
        """
        Initialization of ssh tunnel connection in order to use it to create the Elasticsearch client
        """
        if self.connect == "key":
            self.tunnel = SSHTunnelForwarder(
                ssh_address=self.host,
                ssh_port=self.ssh_port,
                ssh_username=self.ssh_username,
                ssh_private_key=self.ssh_key,
                remote_bind_address=('127.0.0.1', self.port),
                compression=True
            )
        else:
            self.tunnel = SSHTunnelForwarder(
                ssh_address=self.host,
                ssh_port=self.ssh_port,
                ssh_username=self.ssh_username,
                ssh_password=self.ssh_password,
                remote_bind_address=('127.0.0.1', self.port),
                compression=True
            )
        self.tunnel.start()

    def stop(self):
        """
        Stop the ssh tunneling
        """
        if self.connected:
            del self.elasticsearch_client
            [t.close() for t in threading.enumerate() if t.__class__.__name__ == "Transport"]
            self.tunnel.stop()

    def truncate_elasticsearch(self):
        """
        Delete all entries in the elasticsearch
        """
        Search(using=self.elasticsearch_client, index="httpresponses").query("match_all").delete()

    def retrieve_documents(self, previous_date=None) -> List[Dict]:
        """
        Retrieves data from Elasticsearch based on a time range

        Args
            | previous_date (datetime): starting date of the time range

        Returns
            | list: a list of the retrieved documents
        """
        date_range = {'gt': previous_date, 'lte': datetime.now()}
        search_articles = Search(using=self.elasticsearch_client, index='articles').filter('range', date=date_range)
        documents = []
        for hit in search_articles.scan():
            document = hit.to_dict()
            document["id"] = hit.meta["id"]
            if not document["content"].startswith(document["title"]):
                document["content"] = document["title"] + "\r\n\r\n" + document["content"]
            documents.append(document)
        return documents
