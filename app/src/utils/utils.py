import re
from pathlib import Path
from os.path import join
from string import punctuation
from typing import List, AnyStr, Tuple, Set, Union

from ellogon import tokeniser


def tokenize(content: AnyStr) -> List[Tuple[AnyStr]]:
    """
    Use of the ellogon library to split a string into sentences. Each sentence is a tuple containing the sentence's
    tokens.

    Args
        | content (str): the text to be split

    Returns
        | list: a list of tuples (i.e. the sentences) containing each sentence's tokens
    """
    return list(tokeniser.tokenise(content))


def get_punctuation_symbols() -> Set[AnyStr]:
    """
    Function to get a set with punctuation symbols

    Returns
        | set: a set with all the punctuation symbols
    """
    punc = list(set(punctuation))
    punc += ["´", "«" "»"]
    punc = set(punc)
    return punc


def join_sentences(tokenized_sentences: List[Tuple[AnyStr]]) -> List[AnyStr]:
    """
    Function to create a correct string (punctuation in the correct position - correct spaces)

    Args
        | tokenized_sentences (list): a list of sentences. Each sentence is a tuple with the respective tokens

    Returns
        | list: a list of strings (i.e. the sentences)
    """
    sentences = []
    punc = get_punctuation_symbols()
    for sentence in tokenized_sentences:
        sentence = "".join(w if set(w) <= punc else f" {w}" for w in sentence).lstrip()
        sentence = sentence.replace("( ", " (")
        sentence = sentence.replace("« ", " «")
        sentence = sentence.replace(" »", "» ")
        sentence = sentence.replace('" ', ' "')
        sentence = sentence.replace("\n", " ")
        sentence = re.sub(" +", " ", sentence)
        sentence = sentence.strip()
        sentences.append(sentence)
    return sentences


def append_file(sentences: Union[AnyStr, List[AnyStr]], filepath: AnyStr):
    """
    Function to write a list of sentences into a file (uses append)

    Args
        | sentences (list): a list with the document's sentences
        | filepath (str): full path where the file is stored
    """
    if type(sentences) == list:
        sentences = "\n".join(sentences)
    with open(filepath, "a") as f:
        f.write(sentences)
        if not sentences.endswith("\n"):
            f.write("\n")


def combine_files(parent_folder, from_file, to_file):
    with open(join(parent_folder, from_file), "r") as f:
        content = f.read()
    append_file(sentences=content, filepath=join(parent_folder, to_file))


def get_file_paths(folder_list: List[AnyStr]) -> Union[List, AnyStr]:
    file_paths: List[AnyStr] = []
    if folder_list:
        for folder in folder_list:
            if folder:
                file_paths += [str(x) for x in Path(folder).glob("**/*.txt")]
    if len(file_paths) == 1:
        file_paths: AnyStr = file_paths[0]
    return file_paths
