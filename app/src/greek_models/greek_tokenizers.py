import json
import traceback
from enum import Enum
from os.path import join
from typing import Dict, List, AnyStr, Union

from tokenizers.implementations import ByteLevelBPETokenizer
from transformers import RobertaTokenizer, ElectraTokenizer

from src.utils import utils
from src.utils.config import AppConfig


class SpecialTokens(Enum):
    CLS = "[CLS]"
    SEP = "[SEP]"
    PAD = "[PAD]"
    MASK = "[MASK]"
    UNK = "[UNK]"
    EOS = "[EOS]"

    def __str__(self):
        return str(self.value)

    @staticmethod
    def to_list():
        return [SpecialTokens.PAD, SpecialTokens.CLS, SpecialTokens.SEP, SpecialTokens.MASK, SpecialTokens.UNK,
                SpecialTokens.EOS]

    @staticmethod
    def to_str_list():
        return [str(special_token) for special_token in SpecialTokens.to_list()]


class GreekTokenizer:

    def __init__(self, app_config: AppConfig):
        # get config parameters
        self.app_config: AppConfig = app_config
        self.app_logger = app_config.app_logger
        self.properties: Dict = app_config.properties
        self.models_path: AnyStr = app_config.models_path
        self.tokenizer_path: AnyStr = app_config.tokenizer_path

        # Define vocab size & max length
        self.vocab_size: int = self.properties["tokenizer"]["vocab_size"]
        self.max_length: int = self.properties["tokenizer"]["max_len"]
        self.model_name = self.properties["model"]["name"]

        # define tokenizer filenames
        self.vocab_filename: AnyStr = "vocab.json" if self.model_name == "roberta" else "vocab.txt"
        self.merges_filename: AnyStr = "merges.txt"

        # Define dataset paths
        self.train_paths = self.app_config.train_initial_dataset_paths
        self.val_paths = self.app_config.test_initial_dataset_paths
        self.train_file_paths: Union[List[AnyStr], AnyStr] = utils.get_file_paths(folder_list=self.train_paths)
        self.val_file_paths: Union[List[AnyStr], AnyStr] = utils.get_file_paths(folder_list=self.val_paths)
        self.tokenizer = None

    def train(self, do_eval: bool = False):
        # Initialize a tokenizer
        self.vocab_filename = "vocab.json"
        self.app_logger.info("Start training tokenizer")
        tokenizer: ByteLevelBPETokenizer = ByteLevelBPETokenizer()
        special_tokens = SpecialTokens.to_str_list()
        # Customize training
        tokenizer.train(files=self.train_file_paths, vocab_size=self.vocab_size, special_tokens=special_tokens)
        # Save files to disk
        tokenizer.save_model(self.tokenizer_path)
        self.app_logger.info("Tokenizer's training finished! Vocab and merges files are saved to disk")
        self.convert_vocab_file()
        if do_eval:
            self.eval_trained_tokenizer()

    def load(self):
        try:
            if self.tokenizer is None:
                self.app_logger.info("Loading trained tokenizer")
                self.tokenizer = self._load_roberta_tokenizer() if self.model_name == "roberta" else \
                    self._load_electra_tokenizer()
            else:
                self.app_logger.info(f"Tokenizer for {self.model_name} model is already loaded")
        except(BaseException, Exception):
            self.app_logger.error(traceback)
            raise Exception("Tokenizer could not be loaded! Make sure that you provide a trained tokenizer!")

    def _load_electra_tokenizer(self):
        return ElectraTokenizer.from_pretrained(self.tokenizer_path)

    def _load_roberta_tokenizer(self):
        return RobertaTokenizer.from_pretrained(self.tokenizer_path)

    def encode(self, examples):
        self.load()
        return self.tokenizer(examples["text"], max_length=self.max_length, truncation=True, padding=True,
                              add_special_tokens=True, pad_to_max_length=True)

    def convert_vocab_file(self):
        vocab_path = join(self.tokenizer_path, self.vocab_filename)
        if vocab_path.endswith(".json"):
            self._vocab_to_txt()
        elif vocab_path.endswith(".txt"):
            self._vocab_to_json()

    def _vocab_to_txt(self):
        vocab_path = join(self.tokenizer_path, self.vocab_filename)
        with open(vocab_path, "r") as f:
            vocab = json.load(f)
        special_tokens = SpecialTokens.to_str_list()
        vocab = list(vocab.keys())
        special_tokens = [token for token in special_tokens if token not in vocab]
        all_vocab = special_tokens + vocab
        self.vocab_filename = "vocab.txt"
        vocab_path = join(self.tokenizer_path, self.vocab_filename)
        with open(join(vocab_path), "w") as f:
            for token in all_vocab:
                f.write(f"{token}\n")

    def _vocab_to_json(self):
        vocab_path = join(self.tokenizer_path, self.vocab_filename)
        with open(vocab_path, "r") as f:
            vocab = f.readlines()
        vocab_dict = {}
        for idx, token in enumerate(vocab):
            vocab_dict[token] = idx
        self.vocab_filename = "vocab.json"
        vocab_path = join(self.tokenizer_path, self.vocab_filename)
        with open(vocab_path, "w") as f:
            f.write(json.dumps(vocab_dict))

    def eval_trained_tokenizer(self):
        from tokenizers.processors import BertProcessing
        vocab_path = join(self.tokenizer_path, self.vocab_filename)
        merges_path = join(self.tokenizer_path, self.merges_filename)
        tokenizer = ByteLevelBPETokenizer(vocab_path, merges_path)
        tokenizer._tokenizer.post_processor = BertProcessing(
            ("</s>", tokenizer.token_to_id("</s>")),
            ("<s>", tokenizer.token_to_id("<s>")),
        )
        tokenizer.enable_truncation(max_length=self.max_length)
        sentence = "Όχι αεροδρόμιο στο Καστέλλι"
        encoding = tokenizer.encode(sentence)
        tokens = encoding.tokens
        self.app_logger.debug(tokens)
        dec_tokens = []
        for token in tokens:
            dec_tokens.append(tokenizer.decode(token))
        self.app_logger.debug(dec_tokens)
