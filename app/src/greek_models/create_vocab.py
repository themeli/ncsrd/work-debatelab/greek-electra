import json
import glob
import random
from pathlib import Path
from os import listdir
from os.path import join

import sentencepiece as spm
from tokenizers import BertWordPieceTokenizer
from tokenizers import AddedToken

class VocabCreator:

    def __init__(self, paths):
        self.paths = paths
        self.ctrl_symbols = ["[PAD]", "[UNK]", "[CLS]", "[SEP]", "[MASK]"]

    def create_vocab(self):
        raise NotImplementedError


class SentencePieceVocab(VocabCreator):

    def __init__(self, paths):
        super(SentencePieceVocab, self).__init__(paths=paths)
        self.MODEL_PREFIX = "tokenizer"  # @param {type: "string"}
        self.VOC_SIZE = 100000  # @param {type:"integer"}
        self.NUM_PLACEHOLDERS = 256  # @param {type:"integer"}
        self.VOC_FNAME = "vocab.txt"  # @param {type:"string"}

    def get_total_examples(self):
        totals = 0
        for path in self.paths:
            folder_files = listdir(path)
            for file in folder_files:
                filepath = join(path, file)
                with open(filepath, "r", encoding="utf-8", errors='ignore') as f:
                    totals += sum(bl.count("\n") for bl in self.blocks(f))
        return totals

    @staticmethod
    def blocks(file, size=65536):
        while True:
            b = file.read(size)
            if not b:
                break
            yield b

    def get_all_files(self):
        files = []
        for path in self.paths:
            files.append(','.join(glob.glob(f'{path}/*.txt')))
        all_files = ",".join(files)
        return all_files

    @staticmethod
    def read_sentencepiece_vocab(filepath):
        voc = []
        with open(filepath, encoding='utf-8') as fi:
            for line in fi:
                voc.append(line.split("\t")[0])
        # skip the first <unk> token
        voc = voc[1:]
        return voc

    @staticmethod
    def parse_sentencepiece_token(token):
        if token.startswith("▁"):
            return token[1:]
        else:
            return "##" + token

    def create_vocab(self):
        filepaths = self.get_all_files()
        total_examples = self.get_total_examples()
        SPM_COMMAND = ('--input={} --model_prefix={} '
                       '--vocab_size={} --input_sentence_size={} '
                       '--shuffle_input_sentence=true '
                       '--bos_id=-1 --eos_id=-1').format(
            filepaths, self.MODEL_PREFIX,
            self.VOC_SIZE - self.NUM_PLACEHOLDERS, total_examples)
        spm.SentencePieceTrainer.Train(SPM_COMMAND)
        snt_vocab = self.read_sentencepiece_vocab("{}.vocab".format(self.MODEL_PREFIX))
        print("Learnt vocab size: {}".format(len(snt_vocab)))
        print("Sample tokens: {}".format(random.sample(snt_vocab, 10)))
        bert_vocab = list(map(self.parse_sentencepiece_token, snt_vocab))
        bert_vocab = self.ctrl_symbols + bert_vocab
        bert_vocab += ["[UNUSED_{}]".format(i) for i in range(self.VOC_SIZE - len(bert_vocab))]
        print(len(bert_vocab))
        with open(self.VOC_FNAME, "w") as fo:
            for token in bert_vocab:
                fo.write(token + "\n")


class HuggingFaceWordPiece(VocabCreator):

    def __init__(self, paths):
        super(HuggingFaceWordPiece, self).__init__(paths=paths)
        self.files = [str(x) for path in paths for x in Path(path).glob("**/*.txt")]

    @staticmethod
    def get_tokenizer():
        return BertWordPieceTokenizer(
            clean_text=False,
            handle_chinese_chars=True,
            strip_accents=False,
            lowercase=True,
        )

    def create_vocab(self):
        tokenizer = self.get_tokenizer()
        tokenizer.train(
            self.files,
            vocab_size=100000,
            min_frequency=2,
            show_progress=True,
            special_tokens=[AddedToken(x) for x in self.ctrl_symbols],
            limit_alphabet=1000,
            wordpieces_prefix="##"
        )
        # save the vocab
        tokenizer.save('./vocab.json')
        with open("./vocab.json", "r") as f:
            content = json.load(f)

        vocab = content["model"]["vocab"]
        tokens = []
        for key in vocab.keys():
            tokens.append(key)
        with open("./vocab.txt", "w") as f:
            for token in tokens:
                f.write(f"{token}\n")