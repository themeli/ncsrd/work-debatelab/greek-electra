import glob
import os
from os import listdir
from os.path import join
from typing import Dict

from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from transformers import RobertaForMaskedLM, DataCollatorForLanguageModeling, TrainingArguments, Trainer, \
    ProgressCallback, PrinterCallback, ElectraConfig, RobertaConfig, ElectraForMaskedLM
from transformers.integrations import TensorBoardCallback
from transformers.trainer_utils import IntervalStrategy

from src.greek_models.greek_datasets import GreekDataset
from src.greek_models.greek_tokenizers import GreekTokenizer
from src.utils.config import AppConfig


class GreekTransformer:

    def __init__(self, app_config: AppConfig, greek_tokenizer: GreekTokenizer, dataset: GreekDataset):
        self.app_config: AppConfig = app_config
        self.app_logger = app_config.app_logger
        # load tokenizer
        self.greek_tokenizer = greek_tokenizer
        self.greek_tokenizer.load()
        self.tokenizer = self.greek_tokenizer.tokenizer
        self.greek_dataset = dataset

        # load model properties
        self.model_properties = self.app_config.properties["model"]
        self.model_name = self.model_properties["name"]
        self.mlm_probability = self.model_properties["mlm_probability"]
        self.train_epochs = self.model_properties["epochs"]
        self.batch_size = self.model_properties["batch_size"]
        self.warmup_steps = self.model_properties["warmup_steps"]
        self.weight_decay = self.model_properties["weight_decay"]
        self.learning_rate = self.model_properties["learning_rate"]

        self.model_path = join(self.app_config.transformer_path, self.model_name)
        self.vocab_size: int = self.app_config.properties["tokenizer"]["vocab_size"]
        self.max_length: int = self.app_config.properties["tokenizer"]["max_len"]

        self.config = self.get_model_config()

    def train(self, resume=None):
        self.app_logger.info(f"Training model {self.model_name} from scratch")
        checkpoints = listdir(self.model_path)
        if not checkpoints:
            model = RobertaForMaskedLM(config=self.config) if self.model_name == "roberta" else \
                ElectraForMaskedLM(config=self.config)
            resume = None
        else:
            list_of_files = glob.glob(f"{self.model_path}/*")  # * means all if need specific format then *.csv
            latest_file = max(list_of_files, key=os.path.getctime)
            self.app_logger.info(f"Loading model from checkpoint {latest_file}")
            model_class = RobertaForMaskedLM if self.model_name == "roberta" else ElectraForMaskedLM
            model = model_class.from_pretrained(latest_file)
            resume = True if resume is None else resume

        self.app_logger.info("Creating DataCollator")
        data_collator = DataCollatorForLanguageModeling(tokenizer=self.tokenizer, mlm_probability=self.mlm_probability)

        self.app_logger.info("Creating training args & trainer")
        training_args = self.get_training_args()
        trainer = self.get_trainer(data_collator=data_collator, model=model, training_args=training_args)

        self.app_logger.info("Start training")
        trainer.train(resume_from_checkpoint=resume)
        self.app_logger.info("Training finished")
        self.app_logger.info(f"Saving trained model to disk. Model's path: {self.model_path}")
        trainer.save_model(self.model_path)

    def eval(self):
        # TODO
        pass

    def get_trainer(self, data_collator, model, training_args):
        return Trainer(
            tokenizer=self.tokenizer,
            model=model,  # the instantiated 🤗 Transformers model to be trained
            args=training_args,  # training arguments, defined above
            data_collator=data_collator,
            train_dataset=self.greek_dataset.train_dataset,
            eval_dataset=self.greek_dataset.test_dataset,
            callbacks=[TensorBoardCallback, ProgressCallback, PrinterCallback],
            compute_metrics=self.compute_metrics
        )

    def get_training_args(self):
        return TrainingArguments(
            output_dir=self.model_path,
            do_train=True,
            do_eval=True,
            evaluation_strategy=IntervalStrategy.STEPS,
            eval_steps=10000,
            load_best_model_at_end=True,
            num_train_epochs=self.train_epochs,
            per_device_train_batch_size=self.batch_size,  # batch size per device during training
            per_device_eval_batch_size=self.batch_size,
            warmup_steps=self.warmup_steps,  # number of warmup steps for learning rate scheduler
            weight_decay=self.weight_decay,  # strength of weight decay
            learning_rate=self.learning_rate,
            report_to=["tensorboard"],
            metric_for_best_model="loss",
            logging_strategy=IntervalStrategy.STEPS,
            logging_first_step=True,
            save_strategy=IntervalStrategy.STEPS,
            save_steps=10000,
            save_total_limit=10,
            dataloader_num_workers=8,
            logging_dir=self.app_config.logs_path  # directory for storing logs
        )

    @staticmethod
    def compute_metrics(pred):
        labels = pred.label_ids
        preds = pred.predictions.argmax(-1)
        precision, recall, f1, _ = precision_recall_fscore_support(labels, preds, average='weighted',
                                                                   zero_division=1)  # none gives score for each class
        acc = accuracy_score(labels, preds)
        return {
            'accuracy': acc,
            'f1': f1,
            'precision': precision,
            'recall': recall
        }

    def get_model_config(self):
        config_properties: Dict = self.model_properties["config"]
        return self._get_roberta_config(config_properties=config_properties) if self.model_name == "roberta" else \
            self._get_electra_config(config_properties=config_properties)

    def _get_electra_config(self, config_properties):
        return ElectraConfig(vocab_size=self.vocab_size,
                             max_position_embeddings=config_properties["max_position_embeddings"],
                             hidden_size=config_properties["hidden_size"],
                             num_attention_heads=config_properties["num_attention_heads"],
                             num_hidden_layers=config_properties["num_hidden_layers"],
                             type_vocab_size=config_properties["type_vocab_size"]
                             )

    def _get_roberta_config(self, config_properties):
        return RobertaConfig(
            vocab_size=self.vocab_size,
            max_position_embeddings=config_properties["max_position_embeddings"],
            hidden_size=config_properties["hidden_size"],
            num_attention_heads=config_properties["num_attention_heads"],
            num_hidden_layers=config_properties["num_hidden_layers"],
            type_vocab_size=config_properties["type_vocab_size"],
        )
