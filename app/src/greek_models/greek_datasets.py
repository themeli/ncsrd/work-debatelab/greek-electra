import traceback
from os.path import join

from datasets import load_dataset, Dataset, load_from_disk, logging

from src.greek_models.greek_tokenizers import GreekTokenizer
from src.utils.config import AppConfig
from src.utils import utils

logging.set_verbosity_debug()


class GreekDataset:

    def __init__(self, app_config: AppConfig, greek_tokenizer: GreekTokenizer, train_path=None, test_path=None):
        self.app_config: AppConfig = app_config
        self.app_logger = self.app_config.app_logger
        self.greek_tokenizer: GreekTokenizer = greek_tokenizer
        self.batch_size = self.app_config.properties["model"]["batch_size"]

        try:
            self.app_logger.info("Loading train and test datasets from disk")
            self.train_dataset, self.test_dataset = self.load_datasets(train_path=train_path, test_path=test_path)
        except(BaseException, Exception):
            self.app_logger.error(traceback)
            self.train_dataset, self.test_dataset = self.create_datasets()

    def load_datasets(self, train_path=None, test_path=None):
        train_path = self.app_config.train_tokenized_datasets_path if not train_path else \
            join(self.app_config.tokenized_data_path, train_path)
        test_path = self.app_config.test_tokenized_datasets_path if not test_path else \
            join(self.app_config.tokenized_data_path, test_path)
        train_dataset = load_from_disk(train_path)
        test_dataset = load_from_disk(test_path)
        return train_dataset, test_dataset

    def create_datasets(self):
        self.app_logger.info("Creating Train and Test Datasets")

        # initialize path to raw datasets
        train_paths = utils.get_file_paths(folder_list=self.app_config.train_initial_dataset_paths)
        test_path = utils.get_file_paths(folder_list=self.app_config.test_initial_dataset_paths)
        test_path = test_path if type(test_path) == str or not test_path else test_path[0]

        # load raw datasets from disk
        self.app_logger.info("Loading train and test datasets from given paths")
        datasets = load_dataset('text', data_files={'train': train_paths, 'test': test_path},
                                cache_dir=self.app_config.cache_dir)
        train_dataset, test_dataset = datasets["train"], datasets["test"]

        # create tokenized dataset
        self.app_logger.info("Tokenizing datasets")
        train_dataset: Dataset = train_dataset.map(self.greek_tokenizer.encode, batched=True, batch_size=16)
        test_dataset: Dataset = test_dataset.map(self.greek_tokenizer.encode, batched=True, batch_size=16)

        train_dataset.set_format(type='torch', columns=["input_ids", "attention_mask", "text"])
        test_dataset.set_format(type='torch', columns=["input_ids", "attention_mask", "text"])

        train_dataset.save_to_disk(self.app_config.train_tokenized_datasets_path)
        test_dataset.save_to_disk(self.app_config.test_tokenized_datasets_path)
        return train_dataset, test_dataset
