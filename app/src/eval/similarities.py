from typing import AnyStr

import numpy as np
import pandas as pd
import torch
from sentence_transformers.evaluation import EmbeddingSimilarityEvaluator, SimilarityFunction
from transformers import ElectraModel, ElectraTokenizer

from src.utils.config import AppConfig


class GreekModel:

    def __init__(self, app_config: AppConfig, transformer_path: AnyStr, tokenizer_path: AnyStr):
        self.app_config = app_config
        self.max_length: int = self.app_config.properties["tokenizer"]["max_len"]
        self.batch_size: int = self.app_config.properties["model"]["batch_size"]
        self.model_path = transformer_path
        self.tokenizer_path = tokenizer_path
        self.model = self.load_model()
        self.tokenizer = self.load_tokenizer()

    def load_tokenizer(self) -> ElectraTokenizer:
        return ElectraTokenizer.from_pretrained(self.tokenizer_path, max_length=self.max_length)

    def load_model(self) -> ElectraModel:
        return ElectraModel.from_pretrained(self.model_path)

    def encode(self, sentences, batch_size, show_progress_bar, convert_to_numpy=True):
        embeddings = []
        for sentence in sentences:
            input_ids = torch.tensor(self.tokenizer.encode(sentence)).unsqueeze(0)  # Batch size 1
            outputs = self.model(input_ids, output_hidden_states=True, return_dict=True)
            # The last hidden-state is the first element of the output tuple
            last_hidden_states = outputs[0].squeeze(0)[0].reshape(1, -1)
            last_hidden_states = last_hidden_states.detach().numpy()
            embeddings.append(last_hidden_states)
        embeddings = np.concatenate(embeddings, axis=0)
        return embeddings


class EmbeddingSimEvaluator:

    def __init__(self, app_config: AppConfig, greek_model: GreekModel, instances_csv_path: AnyStr):
        self.app_config: AppConfig = app_config
        self.app_logger = app_config.app_logger
        self.greek_model = greek_model
        self.pairs_df = self.load_pairs(instances_csv_path=instances_csv_path)

    @staticmethod
    def load_pairs(instances_csv_path: AnyStr) -> pd.DataFrame:
        return pd.read_csv(instances_csv_path, sep="\t", index_col=None, header=0)

    def eval_similarities(self, method: AnyStr = "cosine"):
        self.app_logger.info(f"Running similarity evaluation for {method} similarity")
        sentences1 = list(self.pairs_df["Sentence1"])
        sentences2 = list(self.pairs_df["Sentence2"])
        scores = list(self.pairs_df["Label"].astype(float))
        similarity_function = self.get_similarity_function(method=method)
        dataset_name = "claims"
        embedding_similarity_evaluator = EmbeddingSimilarityEvaluator(sentences1=sentences1, sentences2=sentences2,
                                                                      scores=scores, show_progress_bar=True,
                                                                      name=dataset_name,
                                                                      main_similarity=similarity_function)
        embedding_similarity_evaluator(model=self.greek_model, output_path=self.app_config.results_path)

    @staticmethod
    def get_similarity_function(method: AnyStr = "cosine") -> SimilarityFunction:
        if method == "cosine":
            return SimilarityFunction.COSINE
        elif method == "euclidean":
            return SimilarityFunction.EUCLIDEAN
        elif method == "manhattan":
            return SimilarityFunction.MANHATTAN
        else:
            return SimilarityFunction.COSINE
