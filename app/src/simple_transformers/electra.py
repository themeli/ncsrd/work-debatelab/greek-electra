import logging
from os import getcwd

from simpletransformers.language_modeling import LanguageModelingModel

logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)


class ElectraSimpleTransformers:

    def __init__(self, train_path, test_path):
        self.train_path = train_path
        self.test_path = test_path
        self.train_file = "train.txt"
        self.test_file = "test.txt"

    def train(self):
        train_args = {
            "clean_text": True,
            "handle_chinese_chars": False,
            "strip_accents": False,
            "lowercase": False,
            "manual_seed": 2021,
            "use_cuda": True,

            "reprocess_input_data": False,
            "overwrite_output_dir": True,
            "num_train_epochs": 3,
            "save_eval_checkpoints": True,
            "save_model_every_epoch": False,
            "learning_rate": 2e-4,
            "warmup_steps": 10000,
            "train_batch_size": 64,
            "eval_batch_size": 128,
            "gradient_accumulation_steps": 1,
            "block_size": 128,
            "max_seq_length": 128,
            "dataset_type": "simple",
            "fp16": False,
            "wandb_project": "ELECTRA",
            "wandb_kwargs": {"name": "Electra-Base"},
            "logging_steps": 100,
            "evaluate_during_training": True,
            "evaluate_during_training_steps": 50000,
            "evaluate_during_training_verbose": True,
            "use_cached_eval_features": True,
            "sliding_window": True,
            "vocab_size": 52000,
            "generator_config": {
                "embedding_size": 128,
                "hidden_size": 256,
                "num_hidden_layers": 3,
            },
            "discriminator_config": {
                "embedding_size": 128,
                "hidden_size": 256,
            },
        }

        model = LanguageModelingModel(
            "electra",
            None,
            args=train_args,
            train_files=self.train_file,
        )

        model.train_model(
            self.train_file, eval_file=self.test_file,
        )

        model.eval_model(self.test_file)


if __name__ == '__main__':
    electra = ElectraSimpleTransformers(train_path=getcwd(), test_path=getcwd())
    electra.train()
