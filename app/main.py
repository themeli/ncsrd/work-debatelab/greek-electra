from src.eval.similarities import EmbeddingSimEvaluator, GreekModel
from src.greek_models.greek_datasets import GreekDataset
from src.greek_models.greek_tokenizers import GreekTokenizer
from src.greek_models.greek_transformer import GreekTransformer
from src.utils.config import AppConfig


def get_greek_tokenizer(app_config):
    greek_tokenizer: GreekTokenizer = GreekTokenizer(app_config=app_config)
    tokenizer_properties = app_config.properties["tokenizer"]
    do_train = tokenizer_properties.get("train", False)
    if do_train:
        greek_tokenizer.train()
    return greek_tokenizer


def train_transformer(app_config, greek_tokenizer, greek_dataset):
    model_properties = app_config.properties["model"]
    do_train = model_properties.get("train", False)
    if do_train:
        # Init model & train
        greek_transformer = GreekTransformer(app_config=app_config, greek_tokenizer=greek_tokenizer,
                                             dataset=greek_dataset)
        resume = model_properties.get("resume", None)
        greek_transformer.train(resume=resume)


def run_training(app_config):
    # get tokenizer
    greek_tokenizer = get_greek_tokenizer(app_config=app_config)
    # load datasets
    greek_dataset = GreekDataset(app_config=app_config, greek_tokenizer=greek_tokenizer)
    train_transformer(app_config=app_config, greek_tokenizer=greek_tokenizer, greek_dataset=greek_dataset)


def evaluate_embedding_similarity(app_config):
    eval_prop = app_config.properties.get("eval", None)
    if eval_prop:
        app_config.app_logger.info("Evaluating similarities")
        model_path = eval_prop["model"]
        app_config.app_logger.info(f"Using transformer model: {model_path}")
        tokenizer_path = eval_prop["tokenizer"]

        file_path = eval_prop["file_path"]
        greek_model = GreekModel(app_config=app_config, transformer_path=model_path, tokenizer_path=tokenizer_path)
        embeddings_sim_eval = EmbeddingSimEvaluator(app_config=app_config, greek_model=greek_model,
                                                    instances_csv_path=file_path)
        embeddings_sim_eval.eval_similarities()


def main():
    app_config: AppConfig = AppConfig()
    tasks = app_config.properties.get("tasks", None)
    if "dataset_collect" in tasks:
        from src.data.create_dataset import collect_datasets
        collect_datasets()
    if "preprocessing" in tasks:
        from src.data.preprocess import EllogonPreprocessor
        folders = app_config.properties["datasets"]["train"]
        folders = [item["path"] for item in folders]
        prep = EllogonPreprocessor(folders=folders)
        prep.apply_preprocessing()
    if "vocab" in tasks:
        from src.greek_models.create_vocab import HuggingFaceWordPiece
        folders = app_config.properties["datasets"]["train"]
        folders = [item["path"] for item in folders]
        vocab_creator = HuggingFaceWordPiece(paths=folders)
        vocab_creator.create_vocab()
    if "training" in tasks:
        run_training(app_config)
    if "similarities_eval" in tasks:
        evaluate_embedding_similarity(app_config)


if __name__ == '__main__':
    main()
